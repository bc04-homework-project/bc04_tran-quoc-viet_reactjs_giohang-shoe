import React, { Component } from "react";

export default class GioHang extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} style={{ width: 80 }} />
          </td>
          <td>
            <button
              onClick={() => this.props.handleAmount(item.id, false)}
              className="btn btn-warning"
            >-</button>
            <span className="mx-3">{item.soLuong}</span>
            <button
              onClick={() => this.props.handleAmount(item.id, true)}
              className="btn btn-success"
            >+</button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => this.props.handleDelete(item)}
              handleDelete
              className="btn btn-danger"
            >Remove</button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container py-5">
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Amount</th>
              <th>Estimate</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
          <tfoot>
            <tr>
              <td colSpan={3}></td>
              <td>Total</td>
              <td>
                {this.props.gioHang.reduce((total, currentItem) => {
                  return total + currentItem.soLuong * currentItem.price;
                }, 0)}
              </td>
            </tr>
          </tfoot>
        </table>

        {this.props.gioHang.length == 0 && <p>There is no item in your cart</p>}
      </div>
    );
  }
}
