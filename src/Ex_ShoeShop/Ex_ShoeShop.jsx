import React, { Component } from "react";
import { dataShoe } from "./Data_Shoe";
import ListShoe from "./ListShoe";
import GioHang from "./GioHang";
import DetailShoe from "./DetailShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: "",
    gioHang: [],
  };


  handleDetail = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };

  handleAddToCart = (shoe) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index == -1) {
      let spGioHang = { ...shoe, soLuong: 1 };
      // console.log("spGioHang: ", spGioHang);
      cloneGioHang.push(spGioHang);
    } 
    else {
      cloneGioHang[index].soLuong++;
    }

    this.setState(
      {
        gioHang: cloneGioHang,
      }
    );
  };

  handleAmount = (shoeID, value) => {
    let index = this.state.gioHang.findIndex((shoe) => {
      return shoe.id == shoeID;
    });

    let cloneGioHang = [...this.state.gioHang];

    if (value) {
      cloneGioHang[index].soLuong++;
    } else {
      if (cloneGioHang[index].soLuong > 1) {
        cloneGioHang[index].soLuong--;
      }
    }

    this.setState({
      gioHang: cloneGioHang,
    });
  };

  handleDelete = (shoe) => {
    let index = this.state.gioHang.indexOf(shoe);
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang.splice(index, 1);

    this.setState({
      gioHang: cloneGioHang,
    });
  };
  
  render() {
    // console.log(this.state.gioHang.length);
    return (
      <div className="mb-5">

        <GioHang
          handleAmount={this.handleAmount}
          handleDelete={this.handleDelete}
          gioHang={this.state.gioHang}
        />
        <ListShoe
          data={this.state.shoeArr}
          handleDetail={this.handleDetail}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      
      </div>
    );
  }
}
