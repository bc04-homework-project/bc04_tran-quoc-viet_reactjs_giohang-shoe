import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { name, image, price, shortDescription } = this.props.detail;
    let handleDetail = this.props.handleDetail;

    return (
      <div>
        <div className="card">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p class="card-text">{shortDescription}</p>
            <p href="#" className="btn btn-primary">
              ${price}
            </p>
            <p>
              <button
                onClick={() => this.props.handleAddToCart(this.props.detail)}
                className="btn btn-success"
              >
                Add to cart
              </button>
              <button
              data-toggle="modal" data-target="#myModal"
                onClick={() => handleDetail(this.props.detail)}
                className="btn btn-warning"
              >
                More detail
              </button>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
